package com.handresc1127.java_restassured;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class bookerTest {

    @Test
    public void getBooking(){
        given().
                header("Accept","application/json").
                when().
                get("https://restful-booker.herokuapp.com/booking").
                then().log().all().assertThat().statusCode(200).body(containsString("bookingid"));

    }

    @Test
    public void createBooking(){
        given().
                header("Content-Type","application/json").
                header("Accept","application/json").
                body("{\n" +
                        "    \"firstname\" : \"Henry\",\n" +
                        "    \"lastname\" : \"Correa\",\n" +
                        "    \"totalprice\" : 110000,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2019-12-23\",\n" +
                        "        \"checkout\" : \"2020-01-02\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Breakfast\"\n" +
                        "}").
                when().
                post("https://restful-booker.herokuapp.com/booking").
                then().
                log().all().
                statusCode(200).body("bookingid",notNullValue());
    }

    @Test
    public void consultBookingByID(){
        given().
                accept("application/json").
                param("id",12).log().all().
                when().
                get("https://restful-booker.herokuapp.com/booking").
                then().
                log().all();
    }

    @Test
    public void consultBookingByIDTrue(){
        given().
                accept("application/json").
                pathParam("id","12").log().all().
                when().
                get("https://restful-booker.herokuapp.com/booking/{id}").
                then().
                log().all().
                statusCode(200).
                body("bookingdates.checkin",equalTo("2019-12-23"));
    }
}
